<?php

class WhiteRabbit {
	public function findMedianLetterInFile( $filePath ) {
		return $this->findMedianLetter( $this->parseFile( $filePath ) );
	}

	/**
	 * Parse the input file for letters.
	 *
	 * @param $filePath
	 *
	 * @return resource
	 */
	private function parseFile( $filePath ) {
		return count_chars( strtolower( file_get_contents( $filePath ) ), 1 );
	}

	/**
	 * Return the letter whose occurrences are the median.
	 *2
	 * @param $parsedFile
	 *
	 * @return mixed
	 * @internal param $occurrences
	 *
	 */
	private function findMedianLetter( $parsedFile ) {

		$letters = array();

		foreach ( $parsedFile as $letter => $occurrences ) {
			if ( ctype_alpha( $letter ) ) {
				array_push( $letters, array(
					"letter" => chr( $letter ),
					"count"  => $occurrences
				) );
			}
		}

		usort( $letters, function ( $a, $b ) {
			return $a['count'] - $b['count'];
		} );

		$count = count( $letters );
		$mid   = intval( floor( ( $count - 1 ) / 2 ) );

		if ($letters % 2 == 0) {
			return $letters[$mid+1];
		} else {
			return $letters[$mid];
		}

	}

}
