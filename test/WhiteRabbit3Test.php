<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(0.01, 0.1, 0.1),
            array(0.2304, 0.48, 0.48),
            array(0.2401, 0.49, 0.49),
            array(0.25, 0.50, 0.50),
            array(0.9702, 0.98, 0.99 ),
            array(49, 7, 7)
        );
    }
}
